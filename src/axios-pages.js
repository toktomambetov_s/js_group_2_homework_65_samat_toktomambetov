import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework-65-b039a.firebaseio.com/'
});

export default instance;