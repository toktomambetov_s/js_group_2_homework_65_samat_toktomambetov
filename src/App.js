import React, { Component } from 'react';
import Layout from "./components/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import Page from "./components/Page/Page";
import Admin from "./components/Admin/Admin";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/admin" exact component={Admin}/>
                <Route path="/:content" exact component={Page}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
