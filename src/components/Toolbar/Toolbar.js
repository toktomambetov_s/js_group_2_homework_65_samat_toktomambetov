import React from 'react';
import {NavLink} from 'react-router-dom';

const Toolbar = () => (
    <header className="Toolbar">
        <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <a className="navbar-brand" href="#">Static Pages</a>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/home">Home<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/about">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/contacts">Contacts</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/divisions">Divisions</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/admin">Admin</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
);

export default Toolbar;