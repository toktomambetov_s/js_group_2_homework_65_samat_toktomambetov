import React, { Component, Fragment } from 'react';
import axios from '../../axios-pages';

class Page extends Component {
    state = {
        title: '',
        content: ''
    };

    _makeRequest() {
        axios.get(this.props.match.params.content + '.json').then(response => {
            this.setState({title: response.data.title, content: response.data.content});
        });
    }

    componentDidMount() {
        console.log(this.props);
        this._makeRequest();
    }

    componentDidUpdate(prevProps) {
        console.log(this.props);
        if (this.props.match.params.content !== prevProps.match.params.content) {
            this._makeRequest();
        }
    }

    render() {
        return (
            <Fragment>
                <h1>{this.state.title}</h1>
                <p>{this.state.content}</p>
            </Fragment>
        );
    }
}

export default Page;