import React, { Component } from 'react';

class Admin extends Component{
    render() {
        return (
            <div>
                <form className="Admin">
                    <label className="mr-sm-2" for="inlineFormCustomSelect">Select page</label>
                    <select className="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
                        <option selected>Choose...</option>
                        <option>Home</option>
                        <option>About</option>
                        <option>Contacts</option>
                        <option>Divisions</option>
                    </select>
                    <div className="form-group">
                        <label>Title</label>
                        <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Title" />
                    </div>
                    <div className="form-group">
                        <label>Content</label>
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"/>
                    </div>
                    <button type="button"  className="btn btn-primary">Save</button>
                </form>
            </div>
        );
    }
};

export default Admin;